package controller;

import API.IManejadorExpresiones;
import model.logic.ManejadorExpresiones;

public class Controller {

	private static IManejadorExpresiones manejador = new ManejadorExpresiones();
	
	public static boolean bienFormada(String bienFormada){
		return manejador.expresionBienFormada(bienFormada);
	}
	
	public static String ordenar(String expresión){
		return manejador.apilar(expresión);
	}
}
