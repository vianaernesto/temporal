package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.ILista;


public class VistaManejadorExpresiones {

	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					System.out.println("Ingrese la expresión");
					
					Scanner scannerExpre = new Scanner(System.in);
					String expresion = scannerExpre.nextLine();
					System.out.println(Controller.bienFormada(expresion));
					break;
				case 2:
					System.out.println("Ingrese la expresión");
					
					Scanner scannerOrde = new Scanner(System.in);
					String expreOrder = scannerOrde.nextLine();
					System.out.println(Controller.ordenar(expreOrder));
					break;
					
				case 3:	
					fin=true;
					break;
			}
			
			
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Expresión bien formada");
		System.out.println("2. Ordenar expresión");
		System.out.println("3. Salir");
		
		
	}
	
	

}
