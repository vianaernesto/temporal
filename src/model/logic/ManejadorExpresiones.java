package model.logic;

import java.util.Vector;

import API.IManejadorExpresiones;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class ManejadorExpresiones implements IManejadorExpresiones{

	public final static String ABRIR = "[(";

	public final static String CERRAR = ")]";

	public boolean expresionBienFormada(String expresion){

		boolean bienFormada =false;
		char[] split = expresion.toCharArray();

		Stack<Character> stack = new Stack<Character>();		
		for(int i =0; i < split.length;i++){
			stack.push(split[i]);
		}

		if(stack.pop() == ']'){
			Character aux = stack.pop();
			if(aux == ')'){
				try	{
					Integer.parseInt(stack.pop()+"");
					Character aux2 = stack.pop();
					if(aux2 == '+'||aux2 == '-'){
						try{
							Integer.parseInt(stack.pop()+"");
							if(stack.pop() == '('){
								if(stack.pop() == '*')
								{
									if(stack.pop() == ')'){
										try
										{
											Integer.parseInt(stack.pop()+"");
											Character aux3 = stack.pop();
											if(aux3 =='+' ||aux3 == '-'){
												try	{
													Integer.parseInt(stack.pop()+"");
													if(stack.pop() == '('){
														if(stack.pop() =='[')
															bienFormada = true;
													}
												}
												catch(Exception e){
													return bienFormada;
												}
											}
										}
										catch(Exception e){
											return bienFormada;
										}
									}

								}
							}
						}
						catch(Exception e){
							return bienFormada;
						}
					}
				}
				catch(Exception e){
					return bienFormada;
				}
			}else{

				try{
					Integer.parseInt(aux.toString());
					try	{
						if(stack.pop() == '*')	{
							if(stack.pop() == ')'){
								try	{
									Integer.parseInt(stack.pop()+"");
									Character aux3 = stack.pop();
									if(aux3 =='+' ||aux3 == '-'){
										try	{
											Integer.parseInt(stack.pop()+"");
											if(stack.pop() == '('){
												if(stack.pop() =='[')
													bienFormada = true;
											}
										}
										catch(Exception e)	{
											return bienFormada;
										}
									}
								}
								catch(Exception e)	{
									return bienFormada;
								}
							}

						}
					}
					catch(Exception e){
						return bienFormada;
					}
				}
				catch(Exception e){
					return bienFormada;
				}
			}
		}
		return bienFormada;
	}

	public String apilar(String expresion){

		String cadena= "";
		if(expresionBienFormada(expresion)){

			char[] split = expresion.toCharArray();

			Stack<Character> stack = new Stack<Character>();		
			for(int i =0; i < split.length;i++){
				stack.push(split[split.length-1-i]);
			}

			stack = ordenarPila(stack);
			for(int i =0; i < split.length;i++){
				cadena = cadena +stack.pop();
			}


		}else{cadena = "La expresi�n ingresada no est� bien formada, por lo cual no se orden�";}

		return cadena;
	}

	public Stack<Character> ordenarPila(Stack<Character> pPila){

		String llaves ="";
		String operadores="";
		String operandos="";
		int tamanio = pPila.size();

		for(int i =0; i <tamanio; i++){

			char aux = pPila.pop();

			if(aux == '['||aux =='('||aux ==')'||aux ==']'){
				 llaves += aux;
			}
			else if(aux =='*'||aux =='+'||aux =='-'){
			      operadores+= aux;

			}else{
				operandos += aux;
			}

		}

		Queue<Character> queue = new Queue<Character>();
		
		char[] llavesS = llaves.toCharArray();
		char[] operadoresS = operadores.toCharArray();
		char[] operandosS = operandos.toCharArray();
		if(llaves != null && operadores != null && operandos != null)
		{
			for(int i =0; i< operandosS.length;i++){
				queue.enqueue(operandosS[i]);
			}
			
			for(int i=0; i < operadoresS.length; i++){
				queue.enqueue(operadoresS[i]);
			}
			
			for(int i =0; i < llavesS.length;i++){
				queue.enqueue(llavesS[i]);
			}

			for(int i = 0; i <tamanio;i++){
				pPila.push(queue.dequeue());
			}

		}
		return pPila;
	}
}

