package API;

public interface IQueue<T> {
	
	
	
	/**
	 * Agrega un item en la �ltima posici�n de la cola
	 * @param item
	 */
	public void enqueue (T item);
	
	/**
	 * Elimina el elemento de la primera posici�n de la cola
	 * @return
	 */
	public T dequeue();
	
	/**
	 * Indica si la cola est� vac�a
	 * @return true si la cola est� vac�a, false de lo contrar�o
	 */
	public boolean isEmpty();
	
	/**
	 * N�mero de elementos en la cola
	 * @return cantidad de elementos que contiene la cola
	 */
	public int size();

}
