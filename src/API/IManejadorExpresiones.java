package API;

public interface IManejadorExpresiones {

	public boolean expresionBienFormada(String expresion);
	
	public String apilar(String expresión);
}
